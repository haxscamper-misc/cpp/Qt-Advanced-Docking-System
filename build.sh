#!/usr/bin/env bash
mkdir -p build
cd build


mkdir -p lib
cd lib
qmake ../../source/lib/qdocksystem.pro
make -j10 --quiet
cd ..

mkdir -p demo
cd demo
qmake ../../source/main/demo.pro
make -j10 --quiet
cd ../..

cp --update source/main/demo .
