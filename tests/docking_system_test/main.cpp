#include <QApplication>
#include <QFile>
#include <QString>
#include <debug_support.hpp>
#include <support.hpp>
#include <dock_manager.hpp>

#include "mainwindow.h"

void __build_fix_never_call() {
    LOG << "asdfasdf";
    //    spt::print("wer");
    ads::DockManager manager(nullptr);
}

static void initStyleSheet(QApplication& a) {
    QFile f(":ads/stylesheets/default-windows.css");
    if (f.open(QFile::ReadOnly)) {
        const QByteArray ba = f.readAll();
        f.close();
        a.setStyleSheet(QString(ba));
    }
}

int main(int argc, char* argv[]) {
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(true);
    initStyleSheet(a);

    MainWindow mw;
    mw.show();
    return a.exec();
}
