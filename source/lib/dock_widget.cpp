#include "dock_widget.hpp"

#include <QApplication>
#include <QBoxLayout>
#include <QDragEnterEvent>
#include <QMenu>
#include <QMimeData>
#include <QMouseEvent>
#include <QPainter>
#include <QPushButton>
#include <QScrollBar>
#include <QSplitter>
#include <QStackedLayout>
#include <QStyle>
#include <QWheelEvent>

#if defined(ADS_ANIMATIONS_ENABLED)
#    include <QGraphicsDropShadowEffect>
#endif

#include "dock_manager.hpp"
#include "drop_overlay.hpp"
#include "floating_widget.hpp"
#include "tab.hpp"
#include "tab_title.hpp"
#include "tab_widget.hpp"
#include <algorithm/all.hpp>

namespace ads {
/// \todo Simplify
DockWidget::DockWidget(DockManager* parent)
    : QFrame(parent)
    , manager(parent)
    , headerLayout(new QHBoxLayout)
    , tabsScrollArea(new TabsScrollArea(this))
    , tabsContainerWidget(new QWidget())
    , tabsLayout(new QHBoxLayout)
    , tabsMenuButton(new QPushButton())
    , closeButton(new QPushButton())
    , contentLayout(new QStackedLayout())
    , tabsLayoutInitCount(0)
    , _mousePressTitleWidget(nullptr)

{
    QBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    /* top area with tabs and close button */

    headerLayout->setContentsMargins(0, 0, 0, 0);
    headerLayout->setSpacing(0);
    mainLayout->addLayout(headerLayout);


    headerLayout->addWidget(tabsScrollArea, 1);


    tabsContainerWidget->setObjectName("tabsContainerWidget");
    tabsScrollArea->setWidget(tabsContainerWidget);


    tabsLayout->setContentsMargins(0, 0, 0, 0);
    tabsLayout->setSpacing(0);
    tabsLayout->addStretch(1);
    tabsContainerWidget->setLayout(tabsLayout);


    tabsMenuButton->setObjectName("tabsMenuButton");
    tabsMenuButton->setFlat(true);
    tabsMenuButton->setIcon(
        style()->standardIcon(QStyle::SP_TitleBarUnshadeButton));
    tabsMenuButton->setMaximumWidth(tabsMenuButton->iconSize().width());
    headerLayout->addWidget(tabsMenuButton, 0);

    // QObject::connect(_tabsMenuButton, &QPushButton::clicked, this,
    // &SectionWidget::onTabsMenuButtonClicked);


    closeButton->setObjectName("closeButton");
    closeButton->setFlat(true);
    closeButton->setIcon(
        style()->standardIcon(QStyle::SP_TitleBarCloseButton));
    closeButton->setToolTip(tr("Close"));
    closeButton->setSizePolicy(
        QSizePolicy::Expanding, QSizePolicy::Expanding);
    headerLayout->addWidget(closeButton, 0);
    QObject::connect(
        closeButton,
        &QPushButton::clicked,
        this,
        &DockWidget::onCloseButtonClicked);


    tabsLayoutInitCount = tabsLayout->count();

    /* central area with contents */

    contentLayout->setContentsMargins(0, 0, 0, 0);
    contentLayout->setSpacing(0);
    mainLayout->addLayout(contentLayout, 1);
}

DockWidget::~DockWidget() {
    ADS_FUNC_BEGIN

    QSplitter* splitter = findParentSplitter(this);
    if (splitter != nullptr && splitter->count() == 0) {
        ADS_LOG << "Removed empty parent splitter";
        splitter->deleteLater();
    }

    ADS_INFO << "Destroyed dock widget";
    ADS_FUNC_END
}


DockManager* DockWidget::getManager() const {
    return manager;
}

QRect DockWidget::headerAreaGeometry() const {
    return headerLayout->geometry();
}

QRect DockWidget::contentAreaGeometry() const {
    return contentLayout->geometry();
}

const std::vector<Tab*> DockWidget::getContents() const {
    return spt::get_raw_ptrs(tabs);
}

void DockWidget::addTab(std::unique_ptr<Tab> tab) {
    TabTitle* title = tab->getTitleWidget();
    tabsLayout->insertWidget(
        tabsLayout->count() - tabsLayoutInitCount, title);

    QObject::connect(
        title, &TabTitle::clicked, this, &DockWidget::onTabTitleClicked);


    TabWidget* content = tab->getTabWidget();
    contentLayout->addWidget(content);
    tab->setDock(this);

    tabs.emplace_back(std::move(tab));


    updateTabsMenu();
}


int DockWidget::indexOfTab(Tab* const tab) const {
    return spt::index_if(tabs, [&](const std::unique_ptr<Tab>& uptr) {
        return uptr.get() == tab;
    });
}


/*!
 * \brief Remove `tab` from all layputs, disconnect signals and clear
 * parents. Call updateTabsMenu()
 * \return TabContent* - the same object as target.
 */
std::unique_ptr<Tab> DockWidget::releaseTab(Tab* _tab ///< Tab to remove
) {
    ADS_FUNC_BEGIN
    int index = indexOfTab(_tab);
    ADS_LOG << "Releasing tab. Old count" << tabs.size();

    std::unique_ptr<Tab> tab = spt::release_uptr(tabs, _tab);

    ADS_LOG << "Releasing tab. New count" << tabs.size();

    TabTitle* title = tab->getTitleWidget();
    headerLayout->removeWidget(title);
    title->disconnect(this);

    TabWidget* content = tab->getTabWidget();
    contentLayout->removeWidget(content);
    content->disconnect(this);

    ADS_LOG << "Removed tab from layout and disconnected signals";

    if (tabs.size() > 0 && title->isActiveTab()) {
        if (index > 0) {
            setCurrentIndex(index - 1);
        } else {
            setCurrentIndex(0);
        }
    }

    updateTabsMenu();

    ADS_FUNC_END
    return tab;
}


int DockWidget::indexOfTabByPos(const QPoint& p, QWidget* exclude) const {
    return spt::index_if(getTabTitles(), [&p, exclude](TabTitle* title) {
        return title->geometry().contains(p)
               && (exclude == nullptr || title != exclude);
    });
}

int DockWidget::currentIndex() const {
    return contentLayout->currentIndex();
}

void DockWidget::moveContent(int from, int to) {
    if (!isValidIndex(from) || !isValidIndex(to) || to < 0 || from == to) {
        qDebug() << "Invalid index for tab movement" << from << to;
        tabsLayout->update();
        return;
    }

    spt::swap_elements(tabs, from, to);

    QLayoutItem* liFrom = nullptr;
    liFrom              = tabsLayout->takeAt(from);
    tabsLayout->insertItem(to, liFrom);


    liFrom = contentLayout->takeAt(from);
    contentLayout->insertWidget(to, liFrom->widget());
    delete liFrom;

    updateTabsMenu();
}

bool DockWidget::isValidIndex(int index) const {
    return index >= 0 && index < tabs.size();
}

int DockWidget::tabCount() const {
    return tabs.size();
}

void DockWidget::showEvent(QShowEvent*) {
    if (!isValidIndex(currentIndex())) {
        return;
    }
    tabsScrollArea->ensureWidgetVisible(getTitle(currentIndex()));
}

void DockWidget::setCurrentIndex(int index) {
    if (!isValidIndex(index)) {
        ADS_ERROR << "Invalid index";
        return;
    }

    /// \todo Simplify code
    for (int i = 0; i < tabsLayout->count(); ++i) {
        QLayoutItem* item = tabsLayout->itemAt(i);
        if (item->widget()) {
            TabTitle* stw = dynamic_cast<TabTitle*>(item->widget());
            if (stw != nullptr) {
                if (i == index) {
                    stw->setActiveTab(true);
                    tabsScrollArea->ensureWidgetVisible(stw);
                } else {
                    stw->setActiveTab(false);
                }
            }
        }
    }

    contentLayout->setCurrentIndex(index);
}

void DockWidget::onTabTitleClicked() {
    TabTitle* stw = qobject_cast<TabTitle*>(sender());
    if (stw) {
        int index = tabsLayout->indexOf(stw);
        setCurrentIndex(index);
    }
}

void DockWidget::onCloseButtonClicked() {
    const int index = currentIndex();
    if (!isValidIndex(index)) {
        return;
    }
    /// \todo Remove tab from vector
}

void DockWidget::onTabsMenuActionTriggered(bool) {
    QAction* a = qobject_cast<QAction*>(sender());
    if (a) {
        const int index = a->data().toInt();
        if (index >= 0) {
            setCurrentIndex(index);
        }
    }
}

void DockWidget::updateTabsMenu() {
    if (tabs.size() == 1) {
        setCurrentIndex(0);
    }

    QMenu* menu = new QMenu();
    for (uint i = 0; i < tabs.size(); ++i) {
        const Tab* tab    = tabs.at(i).get();
        QAction*   action = menu->addAction(QIcon(), tab->getTitle());
        action->setData(i);

        QObject::connect(
            action,
            &QAction::triggered,
            this,
            &DockWidget::onTabsMenuActionTriggered);
    }
    QMenu* old = tabsMenuButton->menu();
    tabsMenuButton->setMenu(menu);
    delete old;
}

std::vector<TabTitle*> DockWidget::getTabTitles() const {
    return spt::get_from_each<TabTitle*>(
        getContents(),
        [](const Tab* tab) { return tab->getTitleWidget(); });
}

TabTitle* DockWidget::getTitle(int index) const {
    return isValidIndex(index) ? tabs.at(index)->getTitleWidget()
                               : nullptr;
}

void DockWidget::setManager(DockManager* value) {
    manager = value;
}

//  //////////////////////////////////////////////////////////////////////
//  TabsScrollArea
//  //////////////////////////////////////////////////////////////////////


TabsScrollArea::TabsScrollArea(QWidget* parent) : QScrollArea(parent) {
    /* Important: QSizePolicy::Ignored makes the QScrollArea behaves
    like a QLabel and automatically fits into the layout. */
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored);
    setFrameStyle(QFrame::NoFrame);
    setWidgetResizable(true);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void TabsScrollArea::wheelEvent(QWheelEvent* e) {
    e->accept();
    const int direction = e->angleDelta().y();

    if (direction < 0) {
        horizontalScrollBar()->setValue(
            horizontalScrollBar()->value() + 20);
    } else {
        horizontalScrollBar()->setValue(
            horizontalScrollBar()->value() - 20);
    }
}

} // namespace ads
